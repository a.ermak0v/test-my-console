<?php

namespace App\Commands;

use MyConsole\Commands\Command;
use MyConsole\Storages\CommandStorage;

/**
 * Команда, которая на вход принимает неограниченное количество аргументов и параметров
 * и выводит их на экранв читаемом для пользователя виде
 */
class ArgumentFormatCommand extends Command
{
    protected ?string $name = 'arg_formatter';

    protected ?string $description = 'Formatted list of arguments and parameters';

    public function execute(): int
    {
        $input = $this->getInput();
        $output= $this->getOutput();
        $arguments = $input->getArguments();
        $options = $input->getOptions();

        $output->writeln();
        $output->writeln('Called command: ' . $this->name);
        $output->writeln();

        $output->writeln('Arguments: ');
        foreach ($arguments as $argument) {
            $output->writeln('  - ' . $argument);
        }

        $output->writeln();
        $output->writeln('Options: ');
        foreach ($options as $name => $values) {
            $output->writeln('  - ' . $name);
            foreach ($values as $value) {
                $output->writeln('    - ' . $value);
            }
        }

        $output->writeln();

        return CommandStorage::SUCCESS;
    }
}
