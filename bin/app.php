<?php

use MyConsole\Application;

/**
 * Точка входа для запуска команд из консоли
 */
if (PHP_SAPI !== 'cli') {
    return;
}

$dirname = dirname(__DIR__);
require_once $dirname . '/vendor/autoload.php';

$commands = require_once $dirname . '/config/commands.php';
(new Application())
    ->addCommands($commands)
    ->run();
