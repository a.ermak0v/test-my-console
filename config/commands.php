<?php

use App\Commands\ArgumentFormatCommand;

/**
 * Конфиг с командоми которые нужно зарегистрировать в приложении
 */
return [
    new ArgumentFormatCommand(),
];
